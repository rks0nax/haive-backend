module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [

    // First application
    {
      name: 'backend',
      script: 'src/index.js',
      error_file: 'logs/err.log',
      out_file: 'logs/out.log',
      merge_logs: true,
      log_date_format: 'YYYY-MM-DD HH:mm Z',
      ignore_watch: ['logs/*', '.git/*'],
      env: {
        COMMON_VARIABLE: 'true',
        DEBUG_COLORS: true,
      },
      env_production: {
        NODE_ENV: 'production',
      },
      args: ['--local', '--color', '-watch'],
      node_args: ['--inspect'],
    },
  ],
};
