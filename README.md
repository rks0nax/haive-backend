# Repo SETUP
- Clone this repo
- Run *npm install*
- Install the following globally *npm install -g*
- - eslint
- - pm2

# Configure your SSH to machine
- Name the Host as *amazon-ec2* in ~/.ssh/config. 
    This is required so that you can make release to the machine
- The user is *ubuntu* and enable ForwardAgent

# GIT SETUP
- copy the origin remote and below lines from *gitconfig* into .git/config
- *git push release* will now push to the server, and run npm install

# Database Configuration
- Just install mongoDB 3.6
- To install run the packages.sh in haive setup

# Release
- Do a *git push release* to push the code to server
- Run ./release.sh to restart the server

# Configure autostart of backend(In the server only)
- Run the command *pm2 start backend*
- Then save it by command *pm2 save*

# PM2 setup local(watch, logs, etc)
- Setup pm2
- run the following command *pm2 start local.config.js*
- The above command will start with watch and talk to local database of mysql
- To tail logs, *pm2 log*