
const crypto = require('crypto');
const uniqid = require('uniqid');
const jwt = require('jsonwebtoken');
const secret = require('./jwt-secret');


/**
 * Generates a uinque random hash
 * @return {string} The unique hash
 */
function uniqueIDGenerator() {
    return crypto.createHash('md5').update(uniqid()).digest('hex');
};

/**
 * Decode the token from authorization header of request
 * or from the Cookie header
 * resolve with token if verified, else reject with error
 * If type is present will match the type and resolve accordingly
 * @param {*} req Express request
 * @param {string} type user type admin | teacher | user
 * Very important, It will not check the type variable in the token and will resolve true
 * for any type
 * @return {Promise} Resolve with decoded token
 */
function verifyTokenFromAuth(req) {
    return new Promise((resolve, reject) => {
        try {
            let authToken = req.cookies.auth || req.get('Authorization').split(' ')[1];
            jwt.verify(authToken, secret, (err, decodedToken) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(decodedToken);
            });
        } catch (e) {
            reject(e);
            return;
        }
    });
};


// Exports

exports.uniqueIDGenerator = uniqueIDGenerator;
exports.verifyTokenFromAuth = verifyTokenFromAuth;
