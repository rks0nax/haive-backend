const generalErrorResponse = require('../response-message/general-error');

// Import the model
const InstituteModel = require('../db/models/institution');
// Utilities import
const authenticateUser = require('../cms/core-utilties').authenticateUser;

/**
 * The middleware that authenticates the super admin
 * @param {ExpressRequest} req
 * @param {ExpressResponse} res
 * @param {ExpressNext} next
 */
function haiveAdminAuth(req, res, next) {
    authenticateUser(req, res).then(() => {
        if (req['user_data'].userType.includes('haiveAdmin')) {
            next();
        } else {
            generalErrorResponse(res, 403, 'Unauthorized');
        }
    });
}

/**
 * The middleware that authenticates the super admin
 * @param {ExpressRequest} req
 * @param {ExpressResponse} res
 * @param {ExpressNext} next
 */
function superAdminAuth(req, res, next) {
    authenticateUser(req, res).then(() => {
        if (req['user_data'].userType.includes('superAdmin')) {
            next();
        } else {
            generalErrorResponse(res, 403, 'Unauthorized');
        }
    });
}

/**
 * The middleware that authenticates the admin
 * @param {ExpressRequest} req
 * @param {ExpressResponse} res
 * @param {ExpressNext} next
 */
function adminAuth(req, res, next) {
    authenticateUser(req, res).then(() => {
        if (req['user_data'].userType.includes('admin') || req['user_data'].userType.includes('superAdmin')) {
            next();
        } else {
            generalErrorResponse(res, 403, 'Unauthorized');
        }
    });
}

/**
 * The middleware that authenticates the teacher
 * @param {ExpressRequest} req
 * @param {ExpressResponse} res
 * @param {ExpressNext} next
 */
function teacherAuth(req, res, next) {
    authenticateUser(req, res).then(() => {
        if (req['user_data'].userType.includes('teacher')) {
            next();
        } else {
            generalErrorResponse(res, 403, 'Unauthorized');
        }
    });
}

/**
 * The middleware that authenticates the student
 * @param {ExpressRequest} req
 * @param {ExpressResponse} res
 * @param {ExpressNext} next
 */
function studentAuth(req, res, next) {
    authenticateUser(req, res).then(() => {
        if (req['user_data'].userType.includes('student')) {
            next();
        } else {
            generalErrorResponse(res, 403, 'Unauthorized');
        }
    });
}

/**
 * The middleware that authenticates any user present in the database
 * @param {ExpressRequest} req
 * @param {ExpressResponse} res
 * @param {ExpressNext} next
 */
function userAuth(req, res, next) {
    authenticateUser(req, res).then(() => {
        next();
    });
}

/**
 * The middleware that checks if admin has no organisation, if true will call next()
 * This requires the admin to be authenticated.
 * i.e. user_data must be present within ExpressRequest
 * @param {any} req Express request
 * @param {any} res Express Response
 * @param {any} next Express next
 */
function adminHasNoInstitute(req, res, next) {
    if (req['user_data']['institutionId'].length > 1) {
        generalErrorResponse(res, 403, 'Institution is already present', 'Only one institution per user');
    } else {
        next();
    }
}

/**
 * The middleware that checks if the admin belongs to an institute
 * This requires the admin to be authenticated
 * and institute data to be present in the request
 * @param {any} req Express request
 * @param {any} res Express Response
 * @param {any} next Express next
 */
function userBelongsToInstitute(req, res, next) {
    if (!req['user_data']) {
        generalErrorResponse(res, 403, 'No authorization');
        return;
    }
    if (!req['institute_data']) {
        generalErrorResponse(res, 403, 'No permissions');
        return;
    };
    if (req['user_data']['institutionId'].includes(req['institute_data']['id'])) {
        next();
        return;
    }
    generalErrorResponse(res, 403, 'Unauthorized admin');
}

/**
 * Requires /:institute param in the path
 * Check whether the institute is verified or not
 * If verified will add 'institute_data' key to request
 * @param {*} req Express request
 * @param {*} res Express response
 * @param {*} next Express next
 */
function instituteVerified(req, res, next) {
    const institute = req.params.institute;
    InstituteModel.findOne({subdomain: institute}).then((instituteData) => {
        if (!instituteData) {
            generalErrorResponse(res, 404, 'Institute does not exist');
            return;
        }
        if (instituteData.verified) {
            req['institute_data'] = instituteData;
            next();
        } else {
            generalErrorResponse(res, 404, 'Institute not verified');
        }
    }, (err) => {
        generalErrorResponse(res, 500, 'Something Went wrong');
    });
}

/**
 * The middleware that checks if user is verified with the mail
 *
 * types - admin | teacher | student
 *
 * This requires the user to be authenticated.
 * i.e. user_data must be present within ExpressRequest
 *
 * @param {any} req Express request
 * @param {any} res Express Response
 * @param {any} next Express next
 */
function userEmailVerified(req, res, next) {
    if (!req['user_data']['verified']['email']) {
        generalErrorResponse(res, 403, 'Email not verified');
        return;
    }
    next();
}

exports.adminMiddlewares = {
    superAdminAuth: superAdminAuth,
    adminAuth: adminAuth,
    haiveAdmin: haiveAdminAuth,
    adminHasNoInstitute: adminHasNoInstitute,
};

exports.instituteMiddlewares = {
    instituteVerified: instituteVerified,
    userBelongsToInstitute: userBelongsToInstitute,
};

exports.teacherMiddlewares = {
    teacherAuth: teacherAuth,
};

exports.studentMiddlewares = {
    studentAuth: studentAuth,
};

exports.userAuth = {
    userAuth: userAuth,
};

exports.coreMiddlewares = {
    userEmailVerified: userEmailVerified,
};
