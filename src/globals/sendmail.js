// Use this to generate Auth token
// https://accounts.zoho.com/apiauthtoken/nb/create?SCOPE=ZohoMail/ZohoMailAPI&EMAIL_ID='email'&PASSWORD='password'
// Use the auth token as Authorization header in  http://mail.zoho.com/api/accounts to get the account id

// Eduhive account
let authToken = 'fbaee5edf6792cfe30725c285108d64a';
let accountId = '7316843000000008002';

const https = require('https');
/**
 * Send mail to the address provided with subject and body
 * @param {string} toAddress Receivers address
 * @param {string} subject The subject of the mail
 * @param {string} content The body of the mail
 * @param {callback} callback callback with the zoho response after sending mail or false if there is an error sending mail
 */
let sendMail = function(toAddress, subject, content, callback) {
    fromAddress = 'support@eduhive.net';
    let postData = {
        'fromAddress': fromAddress,
        'toAddress': toAddress,
        'ccAddress': '',
        'bccAddress': '',
        'subject': subject,
        'content': content,
    };
    let postOptions = {
      host: 'mail.zoho.com',
      port: '443',
      path: '/api/accounts/'+accountId+'/messages',
      method: 'POST',
      headers: {
          'Authorization': authToken,
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(JSON.stringify(postData)),
      },
    };
    let postReq = https.request(postOptions, function(res) {
    res.setEncoding('utf8');
    res.on('data', function(chunk) {
        try {
            chunk = JSON.parse(chunk);
            if (callback) {
                if (chunk.status.code == '200') {
                    callback(chunk.data);
                } else {
                    console.error(chunk);
                    callback(false);
                }
            }
        } catch (e) {
            callback(false);
        }
    });
    });
    postReq.write(JSON.stringify(postData));
    postReq.end();
};

exports.sendMail = sendMail;
