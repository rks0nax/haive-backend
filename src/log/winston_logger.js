let winston = require('winston');

// Log Db files
let dbLogger = new (winston.Logger)({
    transports: [
        new (winston.transports.File)({filename: '../../logs/db.log'}),
    ],
});

// Log registration
let regLogger = new (winston.Logger)({
    transports: [
        new (winston.transports.File)({filename: '../../logs/registration.log'}),
    ],
});

exports.dbLogger = dbLogger;
exports.regLogger = regLogger;
