let dbLogger = require('./winston_logger').db_logger;
/**
 * Responds with error message in the body
 * @param {Response} res The express response object.
 * Must be passed for response to end.
 */
let dbNotConnectedError = function(res) {
    dbLogger.log('error', 'Database not connected');
    if (res) {
        let responseBody = {};
        responseBody.status = 0;
        responseBody.message = 'Database Error';
        res.writeHead(500, {'Content-Type': 'application/json'});
        res.end(JSON.stringify(responseBody));
    }
};

/**
 * This is a general function that responds with message and status code
 * @param {Response} res The express response object
 * @param {*} statusCode The Status Code to send
 * @param {*} message The message to write in the message key of JSON object
 */
let writeResponseError = function(res, statusCode, message) {
    if (!responseBody) {
        responseBody = {};
    }
    responseBody.status = 0;
    responseBody.message = message;
    res.writeHead(statusCode, {'Content-Type': 'application/json'});
    res.end(JSON.stringify(responseBody));
};

/**
 * Respond with error message if database error occurs
 * @param {Response} res The express response object
 */
let dbCollectionError = function(res) {
  dbLogger.log('error', 'Error with update/retrieval from database');
  if (res) {
      let responseBody = {};
      responseBody.status = 0;
      responseBody.message = 'Database Error';
      res.writeHead(501, {'Content-Type': 'application/json'});
      res.end(JSON.stringify(responseBody));
  }
};

// Export Everything here
exports.db_connect_error = dbNotConnectedError;
exports.write_response_error = writeResponseError;
exports.db_col_err = dbCollectionError;
