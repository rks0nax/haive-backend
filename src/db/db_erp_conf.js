module.exports = function(dbName) {
    const localConnectionString = 'mongodb://127.0.0.1:27017/' + dbName;
    // const productionConnectionString = 'mongodb://admin:eKiI7nHctks40msi@haive-shard-00-00-aaxgs.mongodb.net:27017,haive-shard-00-01-aaxgs.mongodb.net:27017,haive-shard-00-02-aaxgs.mongodb.net:27017/'+ dbName +'?ssl=true&replicaSet=Haive-shard-0&authSource=admin';
    const productionConnectionString = 'mongodb://mongo-instance:27017/'+ dbName;
    return (process.argv.indexOf('--local')) > -1 ? localConnectionString : productionConnectionString;
};
