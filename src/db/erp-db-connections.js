
const mongoose = require('mongoose');

let conf = require('./db_erp_conf');
let InstitutionModel = require('./models/institution');

const erpDatabases = {};
// Select all institutions, loop over them and create a sequelize instance
/**
 * Initialize function that returns a promise so that it is awaited before
 * calling the route functions
 * This will initialize all instances of sequelize
 * @return {Promise} A Promise once all db instances are initialized
 */
function init() {
    return new Promise((resolve, reject) => {
        InstitutionModel.find({verified: true}).then((allInstitutes) => {
            /**
             * @param {string} dbName Database name
             * @return {Promise} Promise after connecting to db
             */
            async function createDbConnection(dbName) {
                return new Promise((resolve, reject) => {
                    mongoose.createConnection(conf(dbName)).then((db) => {
                        erpDatabases[dbName] = db;
                        resolve();
                    }, (err) => {
                        reject();
                    });
                });
            }
            (async function() {
                for (let i = 0; i < allInstitutes.length; i++) {
                    let dbName = allInstitutes[i]['subdomain'];
                    await createDbConnection(dbName);
                }
                console.log('All Connections are made to other institutions');
                resolve(true);
            })();
        }, (err) => {
            console.log(err);
            reject();
        });
    });
}

exports.dbs = erpDatabases;
exports.init = init;
