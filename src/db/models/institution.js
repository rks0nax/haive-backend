const Schema = require('mongoose').Schema;
const schemaValidator = require('js-schema');
const db = require('../db-connection').db;
const schema = new Schema({
    name: String,
    subdomain: {
        type: String,
        unique: true,
        sparse: true,
    },
    branch: String,
    logo: {
        type: String,
    },
    verified: {
        type: Boolean,
        default: false,
    },
    address: {
        line1: String,
        line2: String,
        city: String,
        state: String,
        zipCode: String,
        country: String,
        phone: [],
    },
    currency: {
        name: String,
        symbol: String,
    },
    email: String,
    academicYear: {
        startYear: Date,
        endYear: Date,
    },
    segments: {},
    config: {},
    dateCreated: {
        type: Date,
        default: Date.now,
    },
    updatedAt: Date,
});
schema.pre('save', function(next) {
    if (this.segments) {
        const streamSchema = schemaValidator({
            'name': String,
            'id': String,
        });
        const boardSchema = schemaValidator({
            'board': String,
            'streams': Array.of(streamSchema),
        });
        const segmentsSchema = schemaValidator(Array.of(boardSchema));
        if (!segmentsSchema(this.segments)) {
            next(new Error('Invalid data Format'));
        }
    }
    this.updatedAt = Date.now();
    next();
  });
module.exports = db.model('institution', schema);
