const Schema = require('mongoose').Schema;
const db = require('../db-connection').db;
const schema = new Schema({
    name: String,
    email: {
        type: String,
        unique: true,
    },
    password: String,
    phone: {
        type: Number,
        unique: true,
    },
    userType: [],
    permissions: [],
    institutionId: [],
    verified: {
        email: {
            type: Boolean,
            default: false,
        },
        phone: {
            type: Boolean,
            default: false,
        },
    },
    dateCreated: {
        type: Date,
        default: Date.now,
    },
    updatedAt: Date,
});
schema.pre('save', function(next) {
    this.updatedAt = Date.now();
    next();
  });
module.exports = db.model('users', schema);
