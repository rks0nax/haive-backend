module.exports = function(sequelize, DataTypes) {
    return sequelize.define('studentinvites', {
        id: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        email: {
            type: DataTypes.STRING,
        },
        instituteId: {
            type: DataTypes.STRING,
        },
        invitedby: {
            type: DataTypes.STRING,
        },
        acceptedadmin: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        accepteduser: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        expired: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
    });
};
