const Schema = require('mongoose').Schema;
const db = require('../db-connection').db;
const schema = new Schema({
    hashId: {
        type: String,
    },
    userEmail: {
        type: String,
        unique: true,
    },
});

module.exports = db.model('mailVerify', schema);

