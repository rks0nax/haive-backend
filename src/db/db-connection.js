const mongoose = require('mongoose');
let conf = (process.argv.indexOf('--local')) > -1 ? require('./db_local_node') : require('./db_prod_node');
mongoose.connect(conf);
let db = mongoose.connection;
mongoose.set('debug', true);
/**
 * Returns a promise after trying to connect to db
 * Resolve on db connection
 * Rejects on connection failure
 * @return {Promise} Returns a promise once the connection is done
 */
function resolveDB() {
  return new Promise((resolve, reject) => {
    db.on('error', (err) => {
      reject(err);
    });
    db.once('open', function() {
      resolve();
    });
  });
}

exports.resolveDB = resolveDB;
exports.db = db;
