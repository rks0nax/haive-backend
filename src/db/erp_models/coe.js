const Schema = require('mongoose').Schema;
const db = require('../db-connection').db;
const schema = new Schema({
    eventName: {
        type: String,
        required: true,
    },
    eventDescription: String,
    startDate: {
        type: Date,
        required: true,
    },
    endDate: Date,
    dateCreated: {
        type: Date,
        default: Date.now,
    },
    updatedAt: Date,
});
schema.pre('save', function(next) {
    this.updatedAt = Date.now();
    next();
  });
module.exports = db.model('institution', schema);
