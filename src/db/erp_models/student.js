const Schema = require('mongoose').Schema;
// const schemaValidator = require('js-schema');
const dbs = require('../erp-db-connections').dbs;
const schema = new Schema({
    admission_no: {
        type: String,
        unique: true,
        required: true,
    },
    name: {
        first: String,
        second: String,
        last: String,
    },
    registration_no: String,
    roll_no: String,
    gender: String,
    dob: String,
    aadhaar_no: String,
    address: {
        street1: String,
        street2: String,
        city: String,
        state: String,
        zip_code: String,
    },
    nationality: String,
    place_of_birth: String,
    mother_tongue: String,
    blood_group: String,
    disabilities: Boolean,
    disabilities_details: String,
    dateCreated: {
        type: Date,
        default: Date.now,
    },
    updatedAt: Date,
});

/**
 * Get schema of dbs
 * @param {string} dbName Database name from subdomain name
 * @return {any} Database Model
 */
module.exports = function(dbName) {
    return dbs[dbName].model('subjectCategory', schema);
};
