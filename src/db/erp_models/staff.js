const Schema = require('mongoose').Schema;
// const schemaValidator = require('js-schema');
const dbs = require('../erp-db-connections').dbs;
const schema = new Schema({
    employeeID: {
        type: String,
        unique: true,
        required: true,
    },
    name: {
        first: String,
        second: String,
        last: String,
    },
    email: String,
    phone: String,
    gender: String,
    dob: Date,
    aadhaar_no: String,
    address: String,
    nationality: String,
    place_of_birth: String,
    mother_tongue: String,
    blood_group: String,
    disabilities: Boolean,
    disabilities_details: String,
    qualification: [],
    specialization: String,
    duration: Number,
    institute: [],
    designation: [],
    documentSubmission: [],
    employement: {},
    dateCreated: {
        type: Date,
        default: Date.now,
    },
    updatedAt: Date,
});

/**
 * Get schema of dbs
 * @param {string} dbName Database name from subdomain name
 * @return {any} Database Model
 */
module.exports = function(dbName) {
    return dbs[dbName].model('staff', schema);
};
