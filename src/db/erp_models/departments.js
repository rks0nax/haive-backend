const Schema = require('mongoose').Schema;
const schemaValidator = require('js-schema');
const dbs = require('../erp-db-connections').dbs;
const schema = new Schema({
    segmentId: {
        type: String,
        unique: true,
        required: true,
    },
    departments: [],
    dateCreated: {
        type: Date,
        default: Date.now,
    },
    updatedAt: Date,
});
schema.pre('update', function(next) {
    if (typeof this._update['$set']['segmentId'] !== 'string') {
        next(new Error('Invalid Segment ID'));
    }

    const subDepartmentSchema = schemaValidator({
        name: String,
        id: String,
    });
    const department = schemaValidator({
        'id': String,
        'name': String,
        'subdepartments': Array.of(subDepartmentSchema),
        'designations': Array.of(subDepartmentSchema),
    });
    const departmentSchema = schemaValidator(Array.of(department));
    if (!departmentSchema(this._update['$set']['departments'])) {
        next(new Error('Invalid houses Format'));
    }
    this.updatedAt = Date.now();
    next();
  });

/**
 * Get schema of dbs
 * @param {string} dbName Database name from subdomain name
 * @return {any} Database Model
 */
module.exports = function(dbName) {
    return dbs[dbName].model('departments', schema);
};
