const Schema = require('mongoose').Schema;
const schemaValidator = require('js-schema');
const dbs = require('../erp-db-connections').dbs;
const schema = new Schema({
    segmentId: {
        type: String,
        unique: true,
        required: true,
    },
    classes: [],
    dateCreated: {
        type: Date,
        default: Date.now,
    },
    updatedAt: Date,
});
schema.pre('update', function(next) {
    if (typeof this._update['$set']['segmentId'] !== 'string') {
        next(new Error('Invalid Segment ID'));
    }

    const sectionSchema = schemaValidator({
        name: String,
        program: String,
    });
    const eachClassSchema = schemaValidator({
        'id': String,
        'name': String,
        'section': Array.of(sectionSchema),
    });
    const classSchema = schemaValidator(Array.of(eachClassSchema));
    console.log(this._update['$set']);
    if (!classSchema(this._update['$set']['classes'])) {
        next(new Error('Invalid classes Format'));
    }
    this.updatedAt = Date.now();
    next();
  });

/**
 * Get schema of dbs
 * @param {string} dbName Database name from subdomain name
 * @return {any} Database Model
 */
module.exports = function(dbName) {
    return dbs[dbName].model('classes', schema);
};
