const Schema = require('mongoose').Schema;
const schemaValidator = require('js-schema');
const dbs = require('../erp-db-connections').dbs;
const schema = new Schema({
    segmentId: {
        type: String,
        required: true,
        unique: true,
    },
    programmes: [],
    dateCreated: {
        type: Date,
        default: Date.now,
    },
    updatedAt: Date,
});
schema.pre('update', function(next) {
    if (typeof this._update['$set']['segmentId'] !== 'string') {
        next(new Error('Invalid ID'));
    }

    const eachProgramSchema = schemaValidator({
        'name': String,
        'id': String,
        'subjects': Array.of(String),
        'elective_groups': Array.of(String),
    });
    const programSchema = schemaValidator(Array.of(1, Infinity, eachProgramSchema));

    if (!programSchema(this._update['$set']['programmes'])) {
        next(new Error('Invalid programmes Format'));
    }
    this.updatedAt = Date.now();
    next();
  });

/**
 * Get schema of dbs
 * @param {string} dbName Database name from subdomain name
 * @return {any} Database Model
 */
module.exports = function(dbName) {
    return dbs[dbName].model('programmes', schema);
};
