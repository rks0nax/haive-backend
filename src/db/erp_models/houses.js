const Schema = require('mongoose').Schema;
const schemaValidator = require('js-schema');
const dbs = require('../erp-db-connections').dbs;
const schema = new Schema({
    segmentId: {
        type: String,
        unique: true,
        required: true,
    },
    houses: [],
    dateCreated: {
        type: Date,
        default: Date.now,
    },
    updatedAt: Date,
});
schema.pre('update', function(next) {
    if (typeof this._update['$set']['segmentId'] !== 'string') {
        next(new Error('Invalid Segment ID'));
    }

    const houseSchema = schemaValidator({
        'id': String,
        'name': String,
        'description': String,
    });
    const housesSchema = schemaValidator(Array.of(houseSchema));
    if (!housesSchema(this._update['$set']['houses'])) {
        next(new Error('Invalid houses Format'));
    }
    this.updatedAt = Date.now();
    next();
  });

/**
 * Get schema of dbs
 * @param {string} dbName Database name from subdomain name
 * @return {any} Database Model
 */
module.exports = function(dbName) {
    return dbs[dbName].model('houses', schema);
};
