const Schema = require('mongoose').Schema;
const schemaValidator = require('js-schema');
const dbs = require('../erp-db-connections').dbs;
const schema = new Schema({
    segmentId: {
        type: String,
        unique: true,
        required: true,
    },
    categories: [],
    dateCreated: {
        type: Date,
        default: Date.now,
    },
    updatedAt: Date,
});
schema.pre('update', function(next) {
    if (typeof this._update['$set']['segmentId'] !== 'string') {
        next(new Error('Invalid ID'));
    }

    const eachSubjectCategorySchema = schemaValidator({
        'name': String,
        'id': String,
        'compulsory': Boolean,
    });
    const subjectCategorySchema = schemaValidator(Array.of(eachSubjectCategorySchema));

    if (!subjectCategorySchema(this._update['$set']['categories'])) {
        next(new Error('Invalid categories Format'));
    }
    this.updatedAt = Date.now();
    next();
  });

/**
 * Get schema of dbs
 * @param {string} dbName Database name from subdomain name
 * @return {any} Database Model
 */
module.exports = function(dbName) {
    return dbs[dbName].model('subjectCategory', schema);
};
