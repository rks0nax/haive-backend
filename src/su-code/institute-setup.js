

const generalErrorResponse = require('../response-message/general-error');
const successResponse = require('../response-message/general-success');
const response500 = require('../response-message/5xx').response500;

const InstituteModel = require('../db/models/institution');

/**
 * Admin accepts the institute created by admin and then creates
 * database
 * @param {any} req - Express Request
 * @param {any} res - Express Response
 */
function setupInstitute(req, res) {
    const instituteId = req.body.institute;
    const institeSubdomain = req.body.instituteSubdomain;
    if (!instituteId) {
        generalErrorResponse(res, 405, 'No institute Present');
        return;
    }
    if (!institeSubdomain) {
        generalErrorResponse(res, 405, 'No institute subdomain name');
        return;
    }
    // Find the institution based on the post data instituteId
    InstituteModel.findById(instituteId).then((institute) => {
        if (!institute) {
            generalErrorResponse(res, 404, 'Institute Not Found');
            return;
        }
        // Create a new connection
        InstituteModel.update({_id: instituteId}, {verified: true, subdomain: institeSubdomain, $set: {'config.progress': 0}}).then((rawInstituteData) => {
            if (!rawInstituteData) {
                console.error('Could not update institute, nothing was updated');
                generalErrorResponse(res, 500, 'Nothing was updated');
            }
            successResponse(res, 'Successfully created the institute');
        }, (err) => {
            console.error(err);
            response500(res);
        });
    }, (err) => {
        console.error(err);
        response500(res);
    });
}

exports.setupInstitute = setupInstitute;
