
const haiveCoreHandlers = require('./cms/core').core;
const erpInstituteCoreHandlers = require('./cms/institute-core').core;
const erpInstituteSetupHandlers = require('./cms/institute-core').setupRoutes;
const adminMiddlewares = require('./globals/middlewares').adminMiddlewares;
const instituteMiddlewares = require('./globals/middlewares').instituteMiddlewares;
const userMiddleware = require('./globals/middlewares').userAuth;
// const teacherMiddlewares = require('./globals/middlewares').teacherMiddlewares;
const instituteSetup = require('./su-code/institute-setup').setupInstitute;
// const studentMiddlewares = require('./globals/middlewares').studentMiddlewares;
const coreMiddlewares = require('./globals/middlewares').coreMiddlewares;

// For file uploads
const _multer = require('multer');
const multer = _multer({
});


/**
 * Assigns routes to the express instance pased
 * @param {ExpressInstance} app
 */
const routes = function(app) {
    // Admin erp Registration API -- POST
    app.post('/super_admin_reg', haiveCoreHandlers.superAdminRegistration);

    // Admin erp Login -- POST
    app.post('/login', haiveCoreHandlers.userLogin);

    // Admin ERP Logout -- GET
    app.get('/logout', haiveCoreHandlers.userLogout);

    // Admin get institute data -- GET
    app.get('/get_institutes', adminMiddlewares.superAdminAuth, coreMiddlewares.userEmailVerified, haiveCoreHandlers.getAllInstitutesSuperAdmin);

    // User Verify mail API -- GET
    app.get('/verify/registration/:verificationHash', haiveCoreHandlers.mailVerify);

    // Create Admin password -- POST
    app.post('/admin_reg/:hashId', haiveCoreHandlers.adminSetPassword);

    // Get Hash data -- GET
    app.get('/gethash/:hashId', haiveCoreHandlers.getHashData);

    // Get Subdomains -- GET
    app.get('/getactiveinstitutes', haiveCoreHandlers.getAllActivatedInstitutes);
};

/**
 * Subdomain Handles - Assigns routes to the express instance passed
 * @param {any} app Express Instance
 */
const subdomainRoutes = function(app) {
    const initialPath = '/subdomain/:institute/';

    // Get institute status, verified, setup complete
    app.get(initialPath + 'status', erpInstituteCoreHandlers.institutionStatus);

    // institute specific login
    app.post(initialPath + 'login', instituteMiddlewares.instituteVerified, erpInstituteCoreHandlers.userLogin);

    /** Admin Routes **/

    // Setup the institute details
    app.post(initialPath + 'setup', adminMiddlewares.superAdminAuth, coreMiddlewares.userEmailVerified, instituteMiddlewares.instituteVerified, instituteMiddlewares.userBelongsToInstitute, erpInstituteCoreHandlers.instituteInitialSetup);

    // Add/Update Segments
    app.post(initialPath + 'update_segments', adminMiddlewares.superAdminAuth, coreMiddlewares.userEmailVerified, instituteMiddlewares.instituteVerified, instituteMiddlewares.userBelongsToInstitute, erpInstituteSetupHandlers.addSegments);

    // Basic Create and update standard with modelname
    app.post(initialPath + 'data/:modelname', adminMiddlewares.superAdminAuth, coreMiddlewares.userEmailVerified, instituteMiddlewares.instituteVerified, instituteMiddlewares.userBelongsToInstitute, erpInstituteSetupHandlers.addUpdateDataDB);

    // Basic read option with modelname
    app.get(initialPath + 'data/:modelname/:segmentId', adminMiddlewares.superAdminAuth, coreMiddlewares.userEmailVerified, instituteMiddlewares.instituteVerified, instituteMiddlewares.userBelongsToInstitute, erpInstituteCoreHandlers.getDataDB);

    // Get subjects based on segment ID
    app.get(initialPath + 'subjects/:segmentId', userMiddleware.userAuth, coreMiddlewares.userEmailVerified, instituteMiddlewares.instituteVerified, instituteMiddlewares.userBelongsToInstitute, erpInstituteCoreHandlers.getSubjectsBySegmentID);

    // Get Programmes based on segment ID
    app.get(initialPath + 'programmes/:segmentId', userMiddleware.userAuth, coreMiddlewares.userEmailVerified, instituteMiddlewares.instituteVerified, instituteMiddlewares.userBelongsToInstitute, erpInstituteCoreHandlers.getProgrammesBySegmentID);

    // Get Classes by segment ID
    app.get(initialPath + 'classes/:segmentId', adminMiddlewares.superAdminAuth, coreMiddlewares.userEmailVerified, instituteMiddlewares.instituteVerified, instituteMiddlewares.userBelongsToInstitute, erpInstituteCoreHandlers.getClassesBySegmentId);

    // Add student in bulk
    app.post(initialPath + 'student/upload_bulk/:streamId', adminMiddlewares.superAdminAuth, coreMiddlewares.userEmailVerified, instituteMiddlewares.instituteVerified, instituteMiddlewares.userBelongsToInstitute, erpInstituteSetupHandlers.addStudentsParentsBulk);

    // Add staff in bulk
    app.post(initialPath + 'staff/upload_bulk', adminMiddlewares.superAdminAuth, coreMiddlewares.userEmailVerified, instituteMiddlewares.instituteVerified, instituteMiddlewares.userBelongsToInstitute, multer.single('file'), erpInstituteSetupHandlers.addStaffBulk);

    /** Other Routes **/
    app.post(initialPath + 'add_coe', adminMiddlewares.adminAuth, coreMiddlewares.userEmailVerified, instituteMiddlewares.instituteVerified, instituteMiddlewares.userBelongsToInstitute, erpInstituteCoreHandlers.addtoCalendarOfEvents);
};

/**
 * Routes for haive admins only
 * @param {any} app Express Instance
 */
const haiveAdminRoutes = function(app) {
    // APP POST - Register the institute, create a DB and set it up
    app.post('/su/accept/institute', instituteSetup);
};

exports.routes = routes;
exports.subdomainRoutes = subdomainRoutes;
exports.haiveAdminRoutes = haiveAdminRoutes;
