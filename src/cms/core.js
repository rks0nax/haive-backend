

const bcrypt = require('bcrypt');
// const uniqid = require('uniqid');
const jwt = require('jsonwebtoken');
const secret = require('../globals/jwt-secret');

const generalErrorResponse = require('../response-message/general-error');
const successResponse = require('../response-message/general-success');
const response500 = require('../response-message/5xx').response500;
// Mail Verification
const createSignUpMail = require('../cms/mail-utilities').createSignUpMail;

const UserModel = require('../db/models/users');
const MailVerifyModel = require('../db/models/mailverify');
const InstitutionModel = require('../db/models/institution');

/**
 * Admin Signup Before onboarding an institution
 * @param {Request} req Express Request
 * @param {Response} res Express Response
 */
function superAdminRegistration(req, res) {
    // Check for missing values
    if (!req.body.name || !req.body.email || !req.body.phone || !req.body.instituteName) {
        generalErrorResponse(res, 400, 'Missing Fields');
        return;
    }

    // Insert data
    console.log('Creating document entry for new user -> ' + req.body.email);
    UserModel.create({name: String,
        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        userType: ['superAdmin'],
        permissions: ['*'],
    }, (err, user) => {
        if (err) {
            console.error(err);
            if (err.code == 11000) {
                generalErrorResponse(res, 400, 'Failed Creating User', {errCode: err.code});
            } else {
                response500(res);
            }
            return;
        }
        InstitutionModel.create({name: req.body.instituteName, branch: req.body.branch}).then((instituteData) => {
            user.institutionId = [instituteData.id];
            user.save().then((userDataUpdated) => {
                createSignUpMail(req.body.email).then((data) => {
                    successResponse(res, 'Successfully Created', {userID: user.id, mailSent: data});
                }, (err) => {
                    console.error(err);
                    response500(res);
                });
            }, (err) => {
                console.error(err);
                response500(res);
            });
        }, (err) => {
            console.error(err);
            response500(res);
        });
    });
}

/**
 * Admin Password setup
 * @param {Request} req Express Request
 * @param {Response} res Express Response
 */
function adminSetPassword(req, res) {
    const hash = req.params.hashId;
    const password = req.body.password;
    if (!password) {
        generalErrorResponse(res, 402, 'Missing Password');
        return;
    }
    MailVerifyModel.findOne({hashId: hash}).then((hashData) => {
        if (!hashData) {
            generalErrorResponse(res, 404, 'Invalid Hash');
            return;
        }
        UserModel.findOne({email: hashData.userEmail}).then((admin) => {
            if (!admin) {
                generalErrorResponse(res, 404, 'User not found');
                return;
            }
            bcrypt.hash(password, 10, (err, hashString) => {
                if (err) {
                    console.error(err);
                    response500(res);
                    return;
                }
                admin['password'] = hashString;
                admin['verified']['email'] = true;
                admin.save().then(() => {
                    hashData.remove().then((data) => {
                        successResponse(res, 'Successfully updated the password');
                    }, (err) => {
                        console.error(err);
                        response500(res);
                    });
                }, (err) => {
                    console.error(err);
                    generalErrorResponse(res, 500, 'Couldn\'t set password');
                });
            });
        }, (err) => {
            console.error(err);
            response500(res);
        });
    }, (err) => {
        console.error(err);
    });
}

/**
 * Get email of particular Hash
 * @param {Request} req Express Request
 * @param {Response} res Express Response
 */
function getHashData(req, res) {
    if (!req.params.hashId) {
        generalErrorResponse(res, 404, 'Hash not found');
        return;
    }
    MailVerifyModel.findOne({hashId: req.params.hashId}).then((hashData) => {
        if (!hashData) {
            generalErrorResponse(res, 404, 'Invalid Hash');
            return;
        }
        successResponse(res, hashData['userEmail']);
    }, (err) => {
        console.error(err);
        response500(res);
    });
}

/**
 * User Login
 * @param {Request} req Express Request
 * @param {Response} res Express Response
 */
function userLogin(req, res) {
    if (!req.body.email || !req.body.password) {
        generalErrorResponse(res, 400, 'Missing Fields');
        return;
    }
    const password = req.body.password;
    let userData = {
        iat: Math.floor(Date.now() / 1000) + (60 * 60),
        email: req.body.email,
    };
    UserModel.findOne({email: {$eq: userData.email}}).then((user) => {
        if (!user) {
            generalErrorResponse(res, 401, 'No such user or password', 'Please Validate Credentials');
            return;
        }
        bcrypt.compare(password, user.password, (err, validity) => {
            if (err || !validity) {
                generalErrorResponse(res, 401, 'Invalid Credentials', 'Please Validate Credentials');
            } else {
                req['user_data'] = user;
                const token = jwt.sign(userData, secret);
                // Set Cookie - Test feature
                res.cookie('auth', token, {maxAge: 60*60*1000, domain: '.haive.net'});
                successResponse(res, {token: token,
                        userData: {userID: user.id, instituteData: user.institutionId,
                        name: user.name, email: user.email, userType: user.userType, verified: user.verified},
                        }, 'Successfully Authenticated');
            }
        });
    }, (err) => {
        console.error(err);
        response500(res);
    });
}

/**
 * User Logout
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function userLogout(req, res) {
    res.clearCookie('auth');
    res.end();
}

/**
 * Admin Email Verification before accessing account
 * @param {Request} req Express Request
 * @param {Response} res Express Response
 */
function mailVerify(req, res) {
    const verificationHash = req.params.verificationHash;
    MailVerifyModel.findOne({hashId: verificationHash}).then((data) => {
        if (!data) {
            generalErrorResponse(res, 404, 'Not Found');
            return;
        }
        UserModel.updateOne({email: {$eq: data.userEmail}}, {'verified.email': true}).then((rawResponse) => {
            MailVerifyModel.deleteOne({hashId: {$eq: verificationHash}}).then(() => {
                successResponse(res, 'Verified the email');
            }, (err) => {
                console.error(err);
                successResponse(res, 'Validated the email');
            });
        }, (err) => {
            console.error(err);
            response500(res);
        });
    }, (err) => {
        console.error(err);
        response500(res);
    });
}

/**
 * Get all the institute and their data owned by a super admin
 * Returns an array of instituteData
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function getAllInstitutesSuperAdmin(req, res) {
    const instituteArray = req['user_data']['institutionId'];
    InstitutionModel.find({_id: {$in: instituteArray}}).then((institutes) => {
        successResponse(res, institutes);
    }, (err) => {
        response500(res);
    });
}

/**
 * Get all Activated Institute as an array
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function getAllActivatedInstitutes(req, res) {
    InstitutionModel.find({verified: true}, 'subdomain name -_id').lean().then((instituteData) => {
        successResponse(res, instituteData);
    }, (err) => {
        console.error(err);
        response500();
    });
}
// Module exports section
exports.core = {

    // Registrations
    superAdminRegistration: superAdminRegistration,
    userLogin: userLogin,
    userLogout: userLogout,
    mailVerify: mailVerify,
    getAllInstitutesSuperAdmin: getAllInstitutesSuperAdmin,
    adminSetPassword: adminSetPassword,
    getHashData: getHashData,
    getAllActivatedInstitutes: getAllActivatedInstitutes,
};
