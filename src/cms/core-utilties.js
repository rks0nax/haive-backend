
const uniqid = require('uniqid');
// const jwt = require('jsonwebtoken');
// const secret = require('../globals/jwt-secret');
// const bcrypt = require('bcrypt');
const xlsx = require('node-xlsx');
const verifyTokenFromAuth = require('../globals/utilities').verifyTokenFromAuth;

const generalErrorResponse = require('../response-message/general-error');
const response500 = require('../response-message/5xx').response500;
const successResponse = require('../response-message/general-success');

const UserModel = require('../db/models/users');

/**
 * Creates an invite to join a particular Institute
 *
 * Promise will not reject, there will be an error response sent when anything goes wrong
 *
 * @param {*} req Express Request
 * @param {*} res Express Response
 * @param {*} DataModel The Model of particular invite teacher | student
 * @param {*} type type of invite teacher | student
 * @return {Promise} Resolves after successfully creating an invite
 */
function createERPInvites(req, res, DataModel, type) {
    return new Promise((resolve, reject) => {
        if (!req.body.email) {
            generalErrorResponse(res, 401, 'No email present');
            return;
        }
        const uniqID = uniqid('invite_');
        const invitedBy = req['user_data'].id;
        DataModel.findOrCreate({where: {email: req.body.email, instituteId: req['institute_data'].id}, defaults: {id: uniqID, invitedby: invitedBy, acceptedadmin: true}}).then((userData) => {
            // If new entry is created send mail
            if (userData[1]) {
                const email = userData[0].dataValues.teacheremail;
                if (type === 'teacher') {
                    createTeacherInviteEmail(email, req['institute_data'], req['user_data'], (status) => {
                        resolve({mailSent: status, type: type});
                    });
                } else {
                    createStudentInviteMail(email, req['institute_data'], req['user_data'], (status) => {
                        resolve({mailSent: status, type: type});
                    });
                }
            } else {
                successResponse(res, 'Already Invited');
            }
        }, (err) => {
            console.log(err);
            response500(res);
        });
    });
}

/**
 * This function on authenticating the user, adds to the request
 *
 * **user_data** - The entire row of the authenticated user from the model
 *
 * @param {*} req Express Request
 * @param {*} res Express Response
 * @return {Promise} Promise that resolves on authentication
 */
function authenticateUser(req, res) {
    return new Promise((resolve, reject) => {
        verifyTokenFromAuth(req).then((decodedToken) => {
            UserModel.findOne({email: {$eq: decodedToken.email}}).then((user) => {
                if (user) {
                    req['user_data'] = user;
                    resolve();
                } else {
                    generalErrorResponse(res, 401, 'Invalid Token');
                }
            }, (err) => {
                response500(res);
            });
        }, (err) => {
            generalErrorResponse(res, 401, 'Invalid Token');
        });
    });
};

/**
 * Update the invite of a user, set value true for accepted user and expired
 * @param {*} req Express Request
 * @param {*} res Express Response
 * @param {*} InviteModel The Model teacherinvite | studentinvite
 * @param {string} email email of the invited user
 * @param {string} inviteId id of the invite accepted
 * @param {*} UserModel The model teacher | student
 * @param {*} instituteId The ID of the institute being invited to
 * @return {Promise} Promise that resolves on successful update of the models
 */
function updateInvite(req, res, InviteModel, email, inviteId, UserModel, instituteId) {
    return new Promise((resolve, reject) => {
        req['user_data']['instituteIds'].push(instituteId);
        UserModel.update({instituteIds: req['user_data']['instituteIds']}, {where: {id: req['user_data']['id']}}).then((idsUpdateCount) => {
            InviteModel.update({accepteduser: true, expired: true}, {where: {id: inviteId}}).then((acceptCount) => {
                resolve(true);
            }, (err) => {
                console.error(err);
                generalErrorResponse(res, 500, 'Error updating teacher Accept');
            });
        }, (err) => {
            console.error(err);
            response500(res);
        });
    });
}

/**
 * Function to read single xls, xlsx file, from request parse and return as array
 * @param {*} req Express Request
 * @return {Array} The parsed excel sheet
 */
function loadXlsxRequest(req) {
    if (!req.file) {
        return [false, null];
    }
    // Read buffer from request
    const fBuffer = req.file.buffer;
    try {
        const xlsxData = xlsx.parse(fBuffer);
        if (xlsxData.length < 4) {
            return [false, 'Length too short'];
        }
        // Only take the first sheet and filter out rows on other
        // sheet based on that
        const sheet = xlsxData[0];
        let j = sheet.data.length;
        while (j--) {
            const row = sheet.data[j];
            if (!row.length) {
                // Filter out all the other sheet corresponding to the row
                for (let i = 0; i < xlsxData.length; i++) {
                    xlsxData[i]['data'].splice(j, 1);
                }
            }
        }

        // If any other sheet goes beyond the length of the main sheet
        const dataLengthLimit = xlsxData[0]['data'].length;
        for (let i = 1; i < xlsxData.length; i++) {
            xlsxData[i]['data'].length = dataLengthLimit;
        }
        return [true, xlsxData];
    } catch (e) {
        console.error(e);
        return [false, 'Error Reading File'];
    }
}

exports.createERPInvites = createERPInvites;
exports.authenticateUser = authenticateUser;
exports.updateInvite = updateInvite;
exports.loadXlsxRequest = loadXlsxRequest;
