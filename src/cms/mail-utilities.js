// const Sequelize = require('sequelize');

const sendMail = require('../globals/sendmail').sendMail;

const uniqid = require('../globals/utilities').uniqueIDGenerator;

// Models
const MailVerifyModel = require('../db/models/mailverify');
/**
 * Add/update the entry in mailverify table and send mail
 * @param {string} userEmail email of the user
 * @return {Promise<any>} Resolves on verification creation
 */
function createVerificationMail(userEmail) {
    return new Promise((resolve, reject) => {
        const uniqHashID = uniqid();
        MailVerifyModel.create({
            hashId: uniqHashID,
            userEmail: userEmail,
        }).then((user) => {
            resolve();
        }, (err) => {
            reject(err);
        });
    });
}

/**
 * Send mail after admin is verified by haive superadmin
 * @param {*} userEmail User email
 * @param {*} subdomain Subdomain
 * @return {Promise<any>} Resolves once mail is sent
 */
function sendVerificationMail(userEmail, subdomain) {
    return new Promise((resolve, reject) => {
        MailVerifyModel.findOne({userEmail: userEmail}).lean().then((hashData) => {
            let body = 'To verify your mail, please click on the following link. <br/><br/><br/>';
            body += 'https://' + subdomain + '.eduhive.net/verify/registration/' + hashData.uniqHashID;
            // Send the mail for verification
            sendMail(userEmail, 'Verify your Haive account', body, (data) => {
                resolve(data);
            });
        });
        }, (err) => {
            reject(err);
        });
}

/**
 * Sign up mail when users sign up from landing page
 * @param {string} userEmail email of the user
 * @return {Promise<any>} Resolves on verification creation
 */
function createSignUpMail(userEmail) {
    return new Promise((resolve, reject) => {
        createVerificationMail(userEmail).then(() => {
            let body = 'We\'ve received the request. We\'ll soon get back to you. <br/><br/><br/>';
            // Send the mail for verification
            sendMail(userEmail, 'Thank you for signing up', body, (data) => {
                resolve(data);
            });
        }, (err) => {
            reject(err);
        });
    });
}

/**
 * Send a mail to teacher when they are invited by admin to their college
 * @param {string} teacherEmail Teacher Email Id to which the mail will be sent
 * @param {Object<any>} instituteData Institute data Object
 * @param {Object<any>} adminData Admin Data Object
 * @param {callback} callback callback which returns boolean as its first parameter
 */
function createTeacherInviteMail(teacherEmail, instituteData, adminData, callback) {
    const subject = 'Invitation to join -' + instituteData['name'];
    let body = 'You have been invited by the admin to join ' + instituteData['name'] + '.<br/>';
    body += 'Go to your dashboard to accept the invite';
    sendMail(teacherEmail, subject, body, (mailResponse) => {
        if (callback) {
            console.log(mailResponse);
            callback(!!mailResponse);
        }
    });
}

/**
 * Send a mail to student when they are invited by admin to their college
 * @param {string} studentEmail Teacher Email Id to which the mail will be sent
 * @param {Object<any>} instituteData Institute data Object
 * @param {Object<any>} adminData Admin Data Object
 * @param {callback} callback callback which returns boolean as its first parameter
 */
function createStudentInviteMail(studentEmail, instituteData, adminData, callback) {
    const subject = 'Invitation to join -' + instituteData['name'];
    let body = 'You have been invited by the admin to join ' + instituteData['name'] + '.<br/>';
    body += 'Go to your dashboard to accept the invite';
    sendMail(studentEmail, subject, body, (mailResponse) => {
        if (callback) {
            console.log(mailResponse);
            callback(!!mailResponse);
        }
    });
}

exports.createVerificationMail = createVerificationMail;
exports.createTeacherInviteMail = createTeacherInviteMail;
exports.createStudentInviteMail = createStudentInviteMail;
exports.createSignUpMail = createSignUpMail;
exports.sendVerificationMail = sendVerificationMail;
