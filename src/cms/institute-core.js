const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const secret = require('../globals/jwt-secret');
const response500 = require('../response-message/5xx').response500;
const generalErrorResponse = require('../response-message/general-error');
const successResponse = require('../response-message/general-success');
// const response500 = require('../response-message/5xx').response500;

// Models
const instituteModel = require('../db/models/institution');
const UserModel = require('../db/models/users');

// Utilities
const loadXlsxRequest = require('./core-utilties').loadXlsxRequest;

/**
 * Check if institution exists,
 * response with two parameters {exist: boolean, verified: true}
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function institutionStatus(req, res) {
    instituteModel.findOne({subdomain: req.params.institute}).lean().select('-_id -dateCreated -updatedAt -_v').then((instituteData) => {
        if (!instituteData) {
            generalErrorResponse(res, 404, 'Institute not found');
            return;
        }
        instituteData.exist = true;
        successResponse(res, instituteData);
    }, (err) => {
        response500(res);
    });
}

/**
 * The initial Setup of institute that populates the institute table
 *
 * Other Info - Permitted only to admins
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function instituteInitialSetup(req, res) {
    const instituteData = req['institute_data'];
    const requestBody = req.body;

    // All the keys required in req.body Object
    const requiredKeys = ['address', 'email', 'academicYear'];
    // All the keys required in req.body.address Object
    const requiredAddressKeys = ['line1', 'city', 'state', 'zipCode', 'phone'];
    // All the keys required by academic year
    const requiredAcademicYearKeys = ['startYear', 'endYear'];

    // Check if all the required keys are there in request body
    if (!requiredKeys.every((key) => {
        return Object.keys(requestBody).includes(key);
    })) {
        generalErrorResponse(res, 401, 'Missing Fields');
        return;
    }
    // Check if requestBody.address consist of the following fields
    if (!requiredAddressKeys.every((key) => {
            return Object.keys(requestBody.address).includes(key);
    })) {
        generalErrorResponse(res, 401, 'Missing address Fields');
    }
    // Check if Academic Year keys are present
    if (!requiredAcademicYearKeys.every((key) => {
        return Object.keys(requestBody.academicYear).includes(key);
    })) {
        generalErrorResponse(res, 401, 'Missing academic Year Fields');
    }

    if (requestBody.name) {
        instituteData.name = requestBody.name;
    }
    instituteData.address = {
        'line1': requestBody.address.line1,
        'line2': requestBody.address.line2,
        'city': requestBody.address.city,
        'state': requestBody.address.state,
        'zipCode': requestBody.address.zipCode,
        'country': requestBody.address.country,
        'phone': requestBody.address.phone,
    };
    if (requestBody.currency) {
        instituteData.currency = {
            'name': requestBody.currency.name,
            'symbol': requestBody.currency.symbol,
        };
    }
    instituteData.email = requestBody.email;
    instituteData.academicYear = {
        startYear: requestBody.academicYear.startYear,
        endYear: requestBody.academicYear.endYear,
    };
    instituteData.save().then((data) => {
        successResponse(res, 'Okay');
    }, (err) => {
        console.error(err);
        response500(res);
    });
}

/**
 * Add/update Segments after setting up general info
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function addSegments(req, res) {
    const segmentData = req.body;
    if (!segmentData) {
        generalErrorResponse(res, 400, 'No data provided');
    }
    if (!typeof segmentData === 'object' && !segmentData.length) {
        generalErrorResponse(res, 400, 'Invalid Data');
    }
    const instituteData = req['institute_data'];
    instituteData.segments = segmentData;
    instituteData.save().then((data) => {
        successResponse(res, 'Successfully Updated');
    }, (err) => {
        console.error(err);
        response500(res, err.message);
    });
}

/**
 * Add/update Subject Categories based on the current segment ID
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function addSubjectCategories(req, res) {
    const dbName = req['institute_data'].subdomain;
    const subjectCategoryModel = require('../db/erp_models/subject-category')(dbName);
    const body = req.body;
    // Set upsert to true, if the segmentid is not present it will create one
    subjectCategoryModel.update({segmentId: body.segmentId}, {$set: {segmentId: body.segmentId, categories: body.categories}}, {upsert: true, setDefaultsOnInsert: true}).then((data) => {
        successResponse(res, 'Successfully updated');
    }, (err) => {
        console.error(err);
        generalErrorResponse(res, 500, 'Something went wrong');
    });
}

/**
 * Add/update Subject Categories based on the current segment ID
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function getSubjectCategoriesBySegmentID(req, res) {
    const instituteData = req['institute_data'];
    const dbName = instituteData.subdomain;
    const subjectCategoryModel = require('../db/erp_models/subject-category')(dbName);

    const segmentId = req.params.segmentId;
    // Set upsert to true, if the segmentid is not present it will create one
    subjectCategoryModel.findOne({segmentId: segmentId}).select('segmentId categories -_id').lean().then((data) => {
        if (data) {
            successResponse(res, data);
        } else {
            successResponse(res, {segmentId: segmentId, categories: []});
        }
    }, (err) => {
        console.error(err);
        generalErrorResponse(res, 500, 'Something went wrong');
    });
}

/**
 * Add/update Subjects based on the current segment ID
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function addSubjects(req, res) {
    const dbName = req['institute_data'].subdomain;
    const subjectCategoryModel = require('../db/erp_models/subjects')(dbName);
    const body = req.body;
    if (!body.segmentId || !body.subjects) {
        generalErrorResponse(res, 405, 'Missing Fields');
        return;
    }

    // Set upsert to true, if the segmentid is not present it will create one
    subjectCategoryModel.update({segmentId: body.segmentId}, {$set: {segmentId: body.segmentId, subjects: body.subjects}}, {upsert: true, setDefaultsOnInsert: true}).then((data) => {
        successResponse(res, 'Successfully updated');
    }, (err) => {
        console.error(err);
        generalErrorResponse(res, 500, 'Something went wrong');
    });
}

/**
 * GetSubjects based on the current segment ID
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function getSubjectsBySegmentID(req, res) {
    const dbName = req['institute_data'].subdomain;
    const subjectCategoryModel = require('../db/erp_models/subjects')(dbName);
    if (!req.params.segmentId) {
        generalErrorResponse(res, 403, 'No subject ID passed');
        return;
    }
    const segID = req.params.segmentId;
    subjectCategoryModel.findOne({segmentId: segID}).select('segmentId subjects -_id').lean().then((data) => {
        if (!data) {
            successResponse(res, {segmentId: segID, subjects: []});
            return;
        }
        successResponse(res, data);
    }, (err) => {
        console.error(err);
        generalErrorResponse(res, 500, 'Something went wrong');
    });
}

/**
 * Add/update programmes based on the current segment ID
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function addProgrammes(req, res) {
    const dbName = req['institute_data'].subdomain;
    const subjectCategoryModel = require('../db/erp_models/programmes')(dbName);
    const body = req.body;
    if (!body.segmentId || !body.programmes) {
        generalErrorResponse(res, 405, 'Missing Fields');
        return;
    }

    // Set upsert to true, if the segmentid is not present it will create one
    subjectCategoryModel.update({segmentId: body.segmentId}, {$set: {segmentId: body.segmentId, programmes: body.programmes}}, {upsert: true, setDefaultsOnInsert: true}).then((data) => {
        successResponse(res, 'Successfully updated');
    }, (err) => {
        console.error(err);
        generalErrorResponse(res, 500, 'Something went wrong');
    });
}

/**
 * Get programmes by segment ID
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function getProgrammesBySegmentID(req, res) {
    const dbName = req['institute_data'].subdomain;
    const programmeModel = require('../db/erp_models/programmes')(dbName);
    if (!req.params.segmentId) {
        generalErrorResponse(res, 403, 'No subject ID passed');
        return;
    }
    const segID = req.params.segmentId;
    programmeModel.findOne({segmentId: segID}).select('segmentId programmes -_id').lean().then((data) => {
        if (!data) {
            successResponse(res, {segmentId: segID, programmes: []});
            return;
        }
        successResponse(res, data);
    }, (err) => {
        console.error(err);
        generalErrorResponse(res, 500, 'Something went wrong');
    });
}

/**
 * Add classes by segment ID
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function addClasses(req, res) {
    const dbName = req['institute_data'].subdomain;
    const ClassModel = require('../db/erp_models/classes')(dbName);
    const body = req.body;
    if (!body.segmentId || !body.classes) {
        generalErrorResponse(res, 405, 'Missing Fields');
        return;
    }

    // Set upsert to true, if the segmentid is not present it will create one
    ClassModel.update({segmentId: body.segmentId}, {$set: {segmentId: body.segmentId, classes: body.classes}}, {upsert: true, setDefaultsOnInsert: true}).then((data) => {
        successResponse(res, 'Successfully updated');
    }, (err) => {
        console.error(err);
        generalErrorResponse(res, 500, 'Something went wrong');
    });
}

/**
 * Get classes by segment ID
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function getClassesBySegmentId(req, res) {
    const dbName = req['institute_data'].subdomain;
    const ClassModel = require('../db/erp_models/classes')(dbName);
    if (!req.params.segmentId) {
        generalErrorResponse(res, 403, 'No subject ID passed');
        return;
    }
    const segID = req.params.segmentId;
    ClassModel.findOne({segmentId: segID}).select('segmentId classes -_id').lean().then((data) => {
        if (!data) {
            successResponse(res, {segmentId: segID, classes: []});
            return;
        }
        successResponse(res, data);
    }, (err) => {
        console.error(err);
        generalErrorResponse(res, 500, 'Something went wrong');
    });
}

/**
 * Later
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function addUpdateDataDB(req, res) {
    const dbName = req['institute_data'].subdomain;
    const modelfile = (req.params.modelname || '').replace(/_/g, '-');
    if (!modelfile) {
        generalErrorResponse(res, 401, 'Unauthorized');
        return;
    }
    let Model;
    try {
        Model = require('../db/erp_models/' + modelfile)(dbName);
    } catch (e) {
        response500(res);
    }
    const body = req.body;
    if (!body.segmentId) {
        generalErrorResponse(res, 405, 'Missing Fields');
        return;
    }

    // Set upsert to true, if the segmentid is not present it will create one
    Model.update({segmentId: body.segmentId}, {$set: body}, {upsert: true, setDefaultsOnInsert: true}).then((data) => {
        successResponse(res, 'Successfully updated');
    }, (err) => {
        console.error(err);
        generalErrorResponse(res, 500, 'Something went wrong');
    });
}

/**
 * Get data by segment ID based on modelname param
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function getDataDB(req, res) {
    const dbName = req['institute_data'].subdomain;
    const modelfile = (req.params.modelname || '').replace(/_/g, '-');
    if (!modelfile) {
        generalErrorResponse(res, 401, 'Unauthorized');
        return;
    }
    let Model;
    try {
        Model = require('../db/erp_models/' + modelfile)(dbName);
    } catch (e) {
        response500(res);
    }
    if (!req.params.segmentId) {
        generalErrorResponse(res, 403, 'No segment ID passed');
        return;
    }
    const segID = req.params.segmentId;
    Model.findOne({segmentId: segID}).select('-_id -dateCreated -__v').lean().then((data) => {
        successResponse(res, data);
    }, (err) => {
        console.error(err);
        generalErrorResponse(res, 500, 'Something went wrong');
    });
}

/**
 * Bulk upload of excel file for student and parents based on segment ID
 * @param {req} req Express Request
 * @param {res} res Express Response
 */
function addStudentsParentsBulk(req, res) {

}

/**
 * Bulk upload of excel file for Staff based on segment ID
 * @param {req} req Express Request
 * @param {res} res Express Response
 */
function addStaffBulk(req, res) {
    const dbName = req['institute_data'].subdomain;
    const sheetData = loadXlsxRequest(req);
    if (!sheetData[0]) {
        generalErrorResponse(res, 400, sheetData[1]);
        return;
    }
    const sheets = sheetData[1];
    const bulkData = [];
    const StaffModel = require('../db/erp_models/staff')(dbName);
    const personalInfo = sheets[0]['data'];
    const employement = sheets[1]['data'];
    const qualification = sheets[2]['data'];
    const documentSubmission = sheets[3]['data'];
    for (let i = 1; i < personalInfo.length; i++) {
        let staffData = {
            employeeID: personalInfo[i][0],
            name: {
                first: personalInfo[i][1] || '',
                second: personalInfo[i][2] || '',
                last: personalInfo[i][3] || '',
            },
            email: personalInfo[i][4] || '',
            phone: personalInfo[i][5] || '',
            gender: personalInfo[i][6] || '',
            aadhaar_no: personalInfo[i][11] || '',
            address: personalInfo[i][8] || '',
            nationality: personalInfo[i][9] || '',
            place_of_birth: personalInfo[i][10] || '',
            mother_tongue: personalInfo[i][12] || '',
            blood_group: personalInfo[i][13] || '',
            disabilities: personalInfo[i][14] === 'YES',
            disabilities_details: '',

            qualification: qualification[i][3] || '',
            specialization: qualification[i][4] || '',
            duration: qualification[i][5] || '',
            institute: qualification[i][6] || '',
            designation: qualification[i][7] || '',

            documentSubmission: [],

            employement: {
                teachingStaff: false,
                department: employement[i][4],
                subDepartment: employement[i][5],
                designation: employement[i][6],
            },
        };

        let documentSubmissionRow = documentSubmission[i];
        if (documentSubmission[0].length > 3 ) {
            for (let d = 3; d < documentSubmission[0].length; d++) {
                if (documentSubmissionRow[d]) {
                    staffData.documentSubmission.push({
                        name: documentSubmission[0][d],
                        value: documentSubmissionRow[d] === 1,
                    });
                }
            }
        }
        // Set DOB, since it is epoch time
        staffData.dob = new Date(1900, 0, personalInfo[i][7]);
        bulkData.push(staffData);
    }
    StaffModel.insertMany(bulkData).then((err, docs) => {
        if (err) {
            generalErrorResponse(res, 501, 'Could not upload data');
            return;
        }
        successResponse(res, 200, docs.length);
    }, (err) => {
        console.error(err);
        response500(res);
    });
    // Start creating array of staffModel
}

/**
 * Setup calendar of events
 * @param {req} req Express Request
 * @param {res} res Express Response
 */
function addtoCalendarOfEvents(req, res) {
    res.end('OKay');
}

/**
 * Login Endpoint for institute
 * Only users belonging to the specific institute will be permitted
 * @param {*} req Express Request
 * @param {*} res Express Response
 */
function userLogin(req, res) {
    const instituteId = req['institute_data']['id'];
    if (!req.body.email || !req.body.password) {
        generalErrorResponse(res, 400, 'Missing Fields');
        return;
    }
    const password = req.body.password;
    let userData = {
        iat: Math.floor(Date.now() / 1000) + (60 * 60),
        email: req.body.email,
        name: null,
        userType: null,
        permissions: null,
    };
    UserModel.findOne({email: {$eq: userData.email}, institutionId: instituteId}).lean().then((user) => {
        if (!user) {
            generalErrorResponse(res, 401, 'No such user or password', 'Please Validate Credentials');
            return;
        }
        bcrypt.compare(password, user.password, (err, validity) => {
            if (err || !validity) {
                generalErrorResponse(res, 401, 'Invalid Credentials', 'Please Validate Credentials');
            } else {
                req['user_data'] = user;
                userData.userType = user.userType;
                userData.permissions = user.permissions;
                userData.name = user.name;
                const token = jwt.sign(userData, secret);
                // Set Cookie - Test feature
                res.cookie('auth', token, {maxAge: 60*60*10000, domain: req['institute_data']['subdomain'] + '.haive.net'});
                successResponse(res, {token: token,
                        userData: {userID: user.id, instituteData: user.institutionId,
                        name: user.name, email: user.email, userType: user.userType, verified: user.verified},
                        }, 'Successfully Authenticated');
            }
        });
    }, (err) => {
        console.error(err);
        response500(res);
    });
}

exports.core = {
    instituteInitialSetup: instituteInitialSetup,
    getSubjectCategoriesBySegmentID: getSubjectCategoriesBySegmentID,
    getSubjectsBySegmentID: getSubjectsBySegmentID,
    getProgrammesBySegmentID: getProgrammesBySegmentID,
    institutionStatus: institutionStatus,
    addtoCalendarOfEvents: addtoCalendarOfEvents,
    userLogin: userLogin,
    getClassesBySegmentId: getClassesBySegmentId,
    getDataDB: getDataDB,
};

exports.setupRoutes = {
    instituteInitialSetup: instituteInitialSetup,
    addSegments: addSegments,
    addSubjectCategories: addSubjectCategories,
    addSubjects: addSubjects,
    addProgrammes: addProgrammes,
    addSubjectCategories: addSubjectCategories,
    addSegments: addSegments,
    addClasses: addClasses,
    addUpdateDataDB: addUpdateDataDB,
    // User upload
    addStudentsParentsBulk: addStudentsParentsBulk,
    addStaffBulk: addStaffBulk,
};
