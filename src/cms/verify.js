const sequelize = require('../db/db-connection');
const generalErrorResponse = require('../response-message/general-error');
const successResonse = require('../response-message/general-success');

const MailVerify = sequelize.import(__dirname + '/../db/models/mailverify');
const AdminERP = sequelize.import(__dirname + '/../db/models/adminerp');
const Teacher = sequelize.import(__dirname + '/../db/models/teacher');
const Student = sequelize.import(__dirname + '/../db/models/student');
/**
 * Function that takes in the model and verifies the appropriate
 * type of user, Admin, Teacher, Student
 * @param {*} req Express Request
 * @param {*} res Express Response
 * @param {string} type User type. admin | teacher | student
 */
function standardEmailVerifyProcedure(req, res, type) {
    const verificationHash = req.params.verificationHash;
    let DataModel = null;
    switch (type) {
        case 'admin':
            DataModel = AdminERP;
            break;
        case 'teacher':
            DataModel = Teacher;
            break;
        case 'student':
            DataModel = Student;
            break;
    }
    MailVerify.findById(verificationHash).then((userData) => {
        if (!userData) {
            generalErrorResponse(res, 404, 'Not Found');
            return;
        }
        if (userData.type !== type) {
            generalErrorResponse(res, 403, 'Not Found');
            return;
        }
        const userID = userData.userID;
        DataModel.update({emailverified: true}, {where: {id: userID}}).then((updateData) => {
            if (updateData[0] > 0) {
                MailVerify.destroy({where: {id: verificationHash}}).then((destroyedRows) => {
                    successResonse(res, 'Validated ' + type, destroyedRows);
                }, (err) => {
                    successResonse(res, 'Validated ' + type, false);
                });
            } else {
                generalErrorResponse(res, 500, 'Something went wrong', 'Nothing deleted');
            }
        }, (err) => {
            generalErrorResponse(res, 500, 'Something went wrong', err);
        });
    }, (err) => {
        generalErrorResponse(res, 500, 'Something went wrong', err);
    });
}

/**
 * Verify the admin via email
 * @param {*} req Express request
 * @param {*} res Express response
 */
function verifyAdmin(req, res) {
    standardEmailVerifyProcedure(req, res, 'admin');
}

/**
 * Verify the teacher account
 * @param {*} req Express request
 * @param {*} res Express response
 */
function verifyTeacher(req, res) {
    standardEmailVerifyProcedure(req, res, 'teacher');
}

/**
 * Verify the student account
 * @param {*} req Express request
 * @param {*} res Express response
 */
function verifyStudent(req, res) {
    standardEmailVerifyProcedure(req, res, 'student');
}

exports.verifyTeacher = verifyTeacher;
exports.verifyAdmin = verifyAdmin;
exports.verifyStudent = verifyStudent;
