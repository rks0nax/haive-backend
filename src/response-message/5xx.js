/**
 * 500 error response - Mostly internal server error
 * @param {ExpressResponse} res Express Response object
 * @param {string} additionalMessage Any other additional message | Optional
 */
const response500 = (res, additionalMessage) => {
    res.status(500);
    res.set('Content-Type', 'application/json');
    const body = {
        'status': 500,
        'message': 'Something went wrong',
        'other_info': additionalMessage || null,
    };
    res.end(JSON.stringify(body));
};

exports.response500 = response500;
