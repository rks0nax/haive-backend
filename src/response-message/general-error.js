/**
 * Error response with status, message and other_info
 * @param {ExpressResponse} res Express Response object
 * @param {int} status The status code to send
 * @param {string} message The message to send
 * @param {string} additionalMessage Any other additional message
 */
module.exports = (res, status, message, additionalMessage) => {
    res.status(status);
    res.set('Content-Type', 'application/json');
    const body = {
        'status': status,
        'message': message,
        'other_info': additionalMessage || null,
    };
    res.end(JSON.stringify(body));
};
