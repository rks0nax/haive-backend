/**
 * @param {ExpressResponse} res Express Response object
 * @param {string} message The message to send
 * @param {string} additionalMessage Any other additional message
 */
module.exports = (res, message, additionalMessage) => {
    res.status(200);
    res.set('Content-Type', 'application/json');
    const body = {
        'status': 200,
        'message': message,
        'other_info': additionalMessage || null,
    };
    res.end(JSON.stringify(body));
};
