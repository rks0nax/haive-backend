const _express = require('express');
const cookieParser = require('cookie-parser');
const express = require('express')();
let dbSession = require('./db/db-connection').resolveDB;
let erpDbSessions = require('./db/erp-db-connections').init;

dbSession().then(() => {
    console.info('Successfully Connected to the DB');
    erpDbSessions().then(() => {
        // Format all body in json format
        express.use(_express.json());
        express.use(cookieParser());
        require('./routes').routes(express);
        require('./routes').haiveAdminRoutes(express);
        require('./routes').subdomainRoutes(express);
        express.listen(8000);
    }, (err) => {
        console.error('Error initializing other institute databases');
        process.exit(1);
    });
}, (err) => {
    console.error('Failed Connecting To DB');
    console.error(err);
    process.exit(1);
});
